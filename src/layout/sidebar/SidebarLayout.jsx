import {Sidebar} from "flowbite-react";
import React from "react";
import {HiPresentationChartLine, HiOutlineChartBar, HiHome, HiOutlineShoppingCart, HiOutlineCreditCard, HiOutlineUserGroup, HiOutlineFlag} from "react-icons/hi";
import NavbarLayout from "../navbar/NavbarLayout";
import CollapsibleMenuItem from "./CollapsibleMenuItem";
import SingleMenuItem from "./SingleMenuItem";

const SidebarLayout = () => {
    const menu = [
        {
            name: 'dashboard',
            icon: <HiPresentationChartLine />,
        },
         {
            name: 'products',
            icon: <HiHome />,
        },
         {
            name: 'blog',
            icon: <HiOutlineShoppingCart />,
        },
         {
            name: 'transactions',
            icon: <HiOutlineCreditCard />,
        },
         {
            name: 'users',
            icon: <HiOutlineUserGroup />,
        },
         {
            name: 'analysis',
            icon: <HiOutlineChartBar />,
        },
         {
            name: 'reports',
            icon: 'HiOutlineFlag',
             sub: [
                 {
                     name: 'users',
                     icon: <HiOutlineUserGroup />,
                 },
                 {
                     name: 'analysis',
                     icon: <HiOutlineChartBar />,
                 },
             ]
        },
         {
            name: 'investment',
            icon: <HiOutlineFlag />,
        },
         {
            name: 'settings',
            icon: <HiOutlineFlag />,
        },

    ];
    return (
        <>
          <NavbarLayout />
        <div className="w-fit bg-gray-50">
            <Sidebar aria-label="Sidebar with multi-level dropdown example">
                <Sidebar.Items>
                    <Sidebar.ItemGroup>
                        {
                            menu.map(item => item.sub ? <CollapsibleMenuItem item={item} /> : <SingleMenuItem item={item} />)
                        }
                    </Sidebar.ItemGroup>
                </Sidebar.Items>
            </Sidebar>
        </div>

            </>
    )
};

export default SidebarLayout;
