import {Sidebar} from "flowbite-react";
import React from "react";

const CollapsibleMenuItem = ({item}) => (
    <Sidebar.Collapse
        icon={item.icon}
        label={item.name}
    >
        {
            item.subs?.map(sub => (
                <div className='hover:bg-indigo-100 flex '>
                    <div className='flex items-center gap-3 text-sm text-gray-700 font-light px-4 py-3 rounded-lg '>
                        <div>{sub.icon}</div>
                        <div>{sub.name}</div>
                    </div>
                </div>
            ))
        }
    </Sidebar.Collapse>
);

export default CollapsibleMenuItem;
