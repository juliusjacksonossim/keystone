import React from "react";

const SingleMenuItem = ({item}) => (
    <button className='hover:bg-indigo-100 focus:bg-indigo-200 flex rounded w-full'>
        <div className='flex items-center gap-3 text-sm text-gray-700 font-light px-4 py-3 rounded-lg '>
            <div>{item.icon}</div>
            <div>{item.name}</div>
        </div>
    </button>
);

export default SingleMenuItem;
