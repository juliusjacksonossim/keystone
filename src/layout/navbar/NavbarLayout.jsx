import {Avatar, Dropdown, Navbar} from "flowbite-react";
import React from "react";
import SearchBar from "./SearchBar";

const NavbarLayout = () => {
    return (

        <Navbar
            fluid={true}
            rounded={true}
        >

            <div className="flex md:order-last">
                <Dropdown
                    arrowIcon={false}
                    inline={true}
                    label={<Avatar alt="User settings" img="https://flowbite.com/docs/images/people/profile-picture-5.jpg" rounded={true}/>}
                >
                    <Dropdown.Header>
        <span className="block text-sm">
          Bonnie Green
        </span>
                        <span className="block truncate text-sm font-medium">
          name@flowbite.com
        </span>
                    </Dropdown.Header>
                    <Dropdown.Item>
                        Dashboard
                    </Dropdown.Item>
                    <Dropdown.Item>
                        Settings
                    </Dropdown.Item>
                    <Dropdown.Item>
                        Earnings
                    </Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>
                        Sign out
                    </Dropdown.Item>
                </Dropdown>
                <Navbar.Toggle />
            </div>
            <div className='flex flex-wrap justify-between md:pr-7 md:w-4/12'>
                <Navbar.Brand>
                    <h1
                        className="self-center font-display whitespace-nowrap text-7xl text-transparent bg-clip-text bg-gradient-to-r from-slate-900  to-blue-700 font-black dark:text-white"
                    >
                        RETRO
                    </h1>
                </Navbar.Brand>

                <div className="">
                    <Navbar.Collapse >
                        <SearchBar />
                    </Navbar.Collapse>
                </div>
            </div>

        </Navbar>
    )
};

export default NavbarLayout;
